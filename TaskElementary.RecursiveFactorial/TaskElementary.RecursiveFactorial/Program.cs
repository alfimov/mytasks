﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskElementary.RecursiveFactorial
{
    class Program
    {
        static int Factorial(int number) 
        {
            if (number == 1)
            {
                return 1;
            }
            return number * Factorial(number - 1);
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Enter number!");
            int number = int.Parse(Console.ReadLine());
            Console.WriteLine("!{0} = {1}", number, Factorial(number));
        }
    }
}
